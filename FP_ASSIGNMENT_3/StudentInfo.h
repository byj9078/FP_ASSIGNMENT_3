#ifndef STUDENTINFO_H
#define STUDENTINFO_H


#include <cstring>
#include <cstring>
#include <sstream>
#include <iostream>
using namespace std;

class StudentInfo{
public:
    char mName[20];
    unsigned mStudentID;
    float mScore;
    unsigned mAdvisorID;
	StudentInfo() {};
    StudentInfo(char _name[20], unsigned _studentID,
        float _score, unsigned _advisorID);
    StudentInfo(const StudentInfo& _std);
	StudentInfo(string sData);
	~StudentInfo();
	
};
string* SplitByComma(string sData, string split_by_comma[]);
#endif //  STUDENTINFO_H
