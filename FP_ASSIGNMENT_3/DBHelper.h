#ifndef DBHELPER_H
#define DBHELPER_H

#include "DynamicHash.h"
#include "StudentInfo.h"
#include "bplus_tree_db.h"

class DBHelper {
private:
	friend class QueryHelper;
	int mEntrySize;
	string mTableName;
	HashMap* mHashMap;
	BPT* mBplusTree;
public:
	DBHelper(string _table_name);
	~DBHelper();
	void GetStudDataFromFile();
	void GetProfDataFromFile();
	void CreateHashFile();
	//void CreateIndexFile();
	void InsertStudDataIntoDB(StudentInfo _stud);
	void InsertProfDataIntoDB(ProfessorInfo _prof);

	int mSnum_nodes; // for BPT
	int mSroot_num;
	int mPnum_nodes;
	int mProot_num;
};
#define INPUT_STUD_FILE_NAME "sampleData.csv"
#define INPUT_PROF_FILE_NAME "prof_data.csv"
#endif // DBHELPER_H