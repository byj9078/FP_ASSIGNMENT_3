#include "bplus_tree.h"
#define s_block 4096


#ifndef bplus_tree_db_H_INCLUDED
#define bplus_tree_db_H_INCLUDED

void updateNode(string tableName, BPT_Node* node, bool isNew, int &root_num);
void firstInsert(string tableName, float key, int block, int &num_nodes, int &root_num);
void loadBlock(string tableName, int nodeID, BPT_Node* node, int &root_num);
void Insert_Data(string tableName, float key, int block, int &root_num, int &num_nodes, queue<BPT_Node*> &update, queue<BPT_Node*> &n_update);
void RecursiveNode(string tableName, BPT_Node* tmp, float key, int &root_num, int &num_nodes);
void Range_Search(string tableName, float _min, float _max, int &root_num, int &num_nodes);
void ConnectLeaf(string tableName, float _max, int &root_num, BPT_Node* start);
#endif