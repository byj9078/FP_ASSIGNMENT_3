#include "DynamicHash.h"
#include <assert.h>
bool debug = false;
//***************************      Utils     ************************************

/*
************************************Bucket************************************
*/

Bucket::Bucket(int _localdepth, string tablename)
	: mNumEntry(0)
	, mBucketSize(DEFAULT_STUD_BUCKET_SIZE)
	, mLocalDepth(_localdepth)
	, mBlockNumber(-1)
	, mTableName(tablename){
	if (tablename == "Professor") {
		mBucketSize = DEFAULT_PROF_BUCKET_SIZE;
	}
}

void Bucket::SetNumEntry(int _numentry) {
	mNumEntry = _numentry;
}

int Bucket::GetNumEntry() const {
	return mNumEntry;
}

int Bucket::GetBucketSize() const {
	return mBucketSize;
}

bool Bucket::IsFull() const {
	return mNumEntry == mBucketSize;
}
void Bucket::IncreaseNum() {
	mNumEntry++;
}
int Bucket::GetLocalDepth() const {
	return mLocalDepth;
}

void Bucket::SetLocalDepth(int _depth) {
	mLocalDepth = _depth;
}

int Bucket::GetBlockNumber() const {
	return mBlockNumber;
}

void Bucket::SetBlockNumber(int _num) {
	mBlockNumber = _num;
}
string Bucket::GetTableName() const {
	return mTableName;
}
/*
************************************HashMap************************************
*/

HashMap::HashMap(string tablename)
	: mTableName(tablename)
	, mTableSize(DEFAULT_TABLE_SIZE)
	, mGlobalDepth(1)
	, mWholeNumEntry(0) {

	mFileHash.open((mTableName + ".Hash").c_str(), ofstream::binary | ios::out);
	mTable = new Bucket*[mTableSize];
	for (int i = 0; i < mTableSize; i++) {
		mTable[i] = new Bucket(mGlobalDepth, tablename);
		mTable[i]->SetBlockNumber(i);
	}
		
}

HashMap::~HashMap() {
	for (int i = 0; i < mTableSize; i++)
		TableDestructor(i);
	mFileHash.close();


}
void HashMap::TableDestructor(int _tablenum) {
	delete mTable[_tablenum];
}


int HashMap::HashFunc(unsigned _key) {

	return _key % mTableSize;
}

void HashMap::Insert(unsigned _key) {
	int hash = HashFunc(_key);
	if (mTable[hash]->IsFull()) {
		SplitBucket(hash, _key);
		return;
	}
	mWholeNumEntry++;
	mTable[hash]->IncreaseNum();
}
void HashMap::SplitBucket(int _oldtablenum, unsigned _key) {
	if (mTable[_oldtablenum]->GetLocalDepth() == mGlobalDepth) {
		IncreaseHashTable();
		//PrintDBFile();
		Bucket* bucket1 = new Bucket(mGlobalDepth, mTable[0]->GetTableName());
		Bucket* bucket2 = new Bucket(mGlobalDepth, mTable[0]->GetTableName());
		if (mTableName == "Student")
			RedistributeStud(_oldtablenum, bucket1, bucket2);
		else if (mTableName == "Professor")
			RedistributeProf(_oldtablenum, bucket1, bucket2);
	}
	else if (mTable[_oldtablenum]->GetLocalDepth() < mGlobalDepth) {
		int newLocalDepth = mTable[_oldtablenum]->GetLocalDepth() + 1;

		Bucket* bucket1 = new Bucket(newLocalDepth, mTable[0]->GetTableName());
		Bucket* bucket2 = new Bucket(newLocalDepth, mTable[0]->GetTableName());
		if (mTableName == "Student")
			RedistributeStud(_oldtablenum, bucket1, bucket2);
		else if (mTableName == "Professor")
			RedistributeProf(_oldtablenum, bucket1, bucket2);
	}
	Insert(_key);
}
void HashMap::RedistributeStud(int _oldtablenum, Bucket* _bucket1, Bucket* _bucket2) {
	Bucket* oldTable = mTable[_oldtablenum];
	int newLocalDepth = _bucket1->GetLocalDepth();
	int numEntry = oldTable->GetNumEntry();
	StudentInfo* stds = new StudentInfo[numEntry];
	FILE* fp;
	if (debug)
		PrintStudDBFile();
	/* Reading Previous Record's Info */
	fp = fopen((mTableName + ".db").c_str(), "rb");

	fpos_t pos = 4096 * mTable[_oldtablenum]->GetBlockNumber();
	fsetpos(fp, &pos);

	fread(stds, sizeof(StudentInfo), numEntry, fp);
	
	if (debug) {
		for (int i = 0; i < numEntry; i++)
			cout << stds[i].mName << " " << stds[i].mStudentID << " "
			<< stds[i].mScore << " " << stds[i].mAdvisorID << endl;
	}
	if (debug)
		PrintStudDBFile();
	fclose(fp);
	/* Write 0 on Previous Record's location */
	fp = fopen((mTableName + ".db").c_str(), "rb+");
	StudentInfo* zeros = new StudentInfo[numEntry];
	memset(zeros, 0, sizeof(StudentInfo) * numEntry);
	pos = 4096 * mTable[_oldtablenum]->GetBlockNumber();
	fsetpos(fp, &pos);
	fwrite(zeros, sizeof(StudentInfo), numEntry, fp);
	fclose(fp);
	delete[] zeros;
	if (debug)
		PrintStudDBFile();

	/* Distribute Record Into Two Blocks */
	int depthDiff = mGlobalDepth - (newLocalDepth - 1);
	int numOfPointers = (int)pow(2, depthDiff);
	int* pointers = new int[numOfPointers];
	//sort(pointers, numOfPointers);

	int cnt = 0;
	for (int i = 0; i < mTableSize; i++)
		if (mTable[i] == mTable[_oldtablenum]) {
			pointers[cnt++] = i;
			//cout << i << endl;
		}
	int bucket1blockNum = pointers[0] % (int)pow(2, newLocalDepth);
	_bucket1->SetBlockNumber(bucket1blockNum);
	bool first = true;
	for (int i = 0; i < numOfPointers; i++) {
		int newTableNum = pointers[i] % (int)pow(2, newLocalDepth);
		if (newTableNum == bucket1blockNum)
			mTable[pointers[i]] = _bucket1;
		else {
			mTable[pointers[i]] = _bucket2;
			if (first) {
				_bucket2->SetBlockNumber(newTableNum);
				first = false;
			}
				
		}
			
	}

	fp = fopen((mTableName + ".db").c_str(), "rb+");
	for (int i = 0; i < numEntry; i++) {
		int key = stds[i].mStudentID;
		int blockNum = key % (int)pow(2, newLocalDepth);
		
		fpos_t newPos = 4096 * mTable[blockNum]->GetBlockNumber() +32 * mTable[blockNum]->GetNumEntry();
		fsetpos(fp, &newPos);
		fwrite(&stds[i], sizeof(StudentInfo), 1, fp);
		mTable[blockNum]->IncreaseNum();
	}
	fclose(fp);
	if (debug)
		PrintStudDBFile();

	fp = fopen((mTableName + ".db").c_str(), "rb");

	fpos_t zeroPos = 0;
	fsetpos(fp, &zeroPos);
	StudentInfo* temp = new StudentInfo[mTable[0]->GetNumEntry()];
	fread(temp, sizeof(StudentInfo), mTable[0]->GetNumEntry(), fp);

	if (debug) {
		for (int i = 0; i < mTable[0]->GetNumEntry(); i++)
			cout << temp[i].mName << " " << temp[i].mStudentID << " "
			<< temp[i].mScore << " " << temp[i].mAdvisorID << endl;
	}
	fclose(fp);
	delete[] stds;
	delete oldTable;
}
void HashMap::RedistributeProf(int _oldtablenum, Bucket* _bucket1, Bucket* _bucket2) {
	Bucket* oldTable = mTable[_oldtablenum];
	int newLocalDepth = _bucket1->GetLocalDepth();
	int numEntry = oldTable->GetNumEntry();
	ProfessorInfo* profs = new ProfessorInfo[numEntry];
	FILE* fp;
	if (debug)
		PrintProfDBFile();
	/* Reading Previous Record's Info */
	fp = fopen((mTableName + ".db").c_str(), "rb");

	fpos_t pos = 4096 * mTable[_oldtablenum]->GetBlockNumber();
	fsetpos(fp, &pos);

	fread(profs, sizeof(ProfessorInfo), numEntry, fp);

	if (debug) {
		for (int i = 0; i < numEntry; i++)
			cout << profs[i].mName << " " << profs[i].mProfID << " "
			<< profs[i].mSalary << endl;
	}
	if (debug)
		PrintProfDBFile();
	fclose(fp);

	/* Write 0 on Previous Record's location */
	fp = fopen((mTableName + ".db").c_str(), "rb+");
	ProfessorInfo* zeros = new ProfessorInfo[numEntry];
	memset(zeros, 0, sizeof(ProfessorInfo) * numEntry);
	pos = 4096 * mTable[_oldtablenum]->GetBlockNumber();
	fsetpos(fp, &pos);
	fwrite(zeros, sizeof(ProfessorInfo), numEntry, fp);
	fclose(fp);
	delete[] zeros;
	if (debug)
		PrintProfDBFile();

	/* Distribute Record Into Two Blocks */
	int depthDiff = mGlobalDepth - (newLocalDepth - 1);
	int numOfPointers = (int)pow(2, depthDiff);
	int* pointers = new int[numOfPointers];
	//sort(pointers, numOfPointers);

	int cnt = 0;
	for (int i = 0; i < mTableSize; i++)
		if (mTable[i] == mTable[_oldtablenum]) {
			pointers[cnt++] = i;
			//cout << i << endl;
		}
	int bucket1blockNum = pointers[0] % (int)pow(2, newLocalDepth);
	_bucket1->SetBlockNumber(bucket1blockNum);
	bool first = true;
	for (int i = 0; i < numOfPointers; i++) {
		int newTableNum = pointers[i] % (int)pow(2, newLocalDepth);
		if (newTableNum == bucket1blockNum)
			mTable[pointers[i]] = _bucket1;
		else {
			mTable[pointers[i]] = _bucket2;
			if (first) {
				_bucket2->SetBlockNumber(newTableNum);
				first = false;
			}

		}

	}

	fp = fopen((mTableName + ".db").c_str(), "rb+");
	for (int i = 0; i < numEntry; i++) {
		int key = profs[i].mProfID;
		int blockNum = key % (int)pow(2, newLocalDepth);

		fpos_t newPos = 4096 * mTable[blockNum]->GetBlockNumber() + 28 * mTable[blockNum]->GetNumEntry();
		fsetpos(fp, &newPos);
		fwrite(&profs[i], sizeof(ProfessorInfo), 1, fp);
		mTable[blockNum]->IncreaseNum();
	}
	fclose(fp);
	if (debug)
		PrintProfDBFile();
	delete[] profs;
	delete oldTable;
}
void HashMap::IncreaseHashTable() {
	int oldTableSize = mTableSize;
	Bucket** oldTable = mTable;

	mTableSize *= 2;
	mGlobalDepth++;
	mTable = new Bucket*[mTableSize];

	for (int i = 0; i < oldTableSize; i++)
		mTable[i] = oldTable[i];
	for (int i = oldTableSize; i < mTableSize; i++)
		mTable[i] = oldTable[i - mTableSize / 2];

	delete oldTable;
}
void HashMap::PrintStudDBFile() {
	cout << "***************   Print out Hash Table  ****************\n" << endl;
	cout << "\t\t\tGlobal Depth: " << mGlobalDepth << endl;
	
	for (int i = 0; i < mTableSize; i++) {
		cout << "Table[" << i
			<< "]------Local Depth: " << mTable[i]->GetLocalDepth() << "   "
			<< "Block Number : " << mTable[i]->GetBlockNumber() << "-----" << endl;
		FILE* fp = fopen((mTableName + ".db").c_str(), "rb");
		fpos_t pos = 4096 * mTable[i]->GetBlockNumber();
		fsetpos(fp, &pos);
		StudentInfo* stds = new StudentInfo[mTable[i]->GetNumEntry()];
		fread(stds, sizeof(StudentInfo), mTable[i]->GetNumEntry(), fp);
		for (int j = 0; j < mTable[i]->GetNumEntry(); j++) {
			cout << "| " << stds[j].mName << ", "
				<< stds[j].mStudentID << ", "
				<< stds[j].mScore << ", "
				<< stds[j].mAdvisorID << " | ";
			if (j % 2)
				cout << endl;
		}

		cout << "\n---------------------------------------------------------\n";
	}
	cout << "\n****************************************************\n" << endl;
}
void HashMap::PrintProfDBFile() {
	cout << "***************   Print out "
		<< mTableName
		<< " DB FILE  ****************\n" << endl;
	cout << "\t\t\tGlobal Depth: " << mGlobalDepth << endl;

	for (int i = 0; i < mTableSize; i++) {
		cout << "Table[" << i
			<< "]------Local Depth: " << mTable[i]->GetLocalDepth() << "   "
			<< "Block Number : " << mTable[i]->GetBlockNumber() << "-----" << endl;
		FILE* fp = fopen((mTableName + ".db").c_str(), "rb");
		fpos_t pos = 4096 * mTable[i]->GetBlockNumber();
		fsetpos(fp, &pos);
		ProfessorInfo* profs = new ProfessorInfo[mTable[i]->GetNumEntry()];
		fread(profs, sizeof(ProfessorInfo), mTable[i]->GetNumEntry(), fp);
		for (int j = 0; j < mTable[i]->GetNumEntry(); j++) {
			cout << "| " << profs[j].mName << ", "
				<< profs[j].mProfID << ", "
				<< profs[j].mSalary << " | ";
			if (j % 3)
				cout << endl;
		}

		cout << "\n---------------------------------------------------------\n";
	}
	cout << "\n****************************************************\n" << endl;
}

void HashMap::CreateHashFile() {
	FILE* fp = fopen((mTableName + ".db").c_str(), "rb+");

	for (int i = 0; i < mTableSize; i++) {
		int bn = mTable[i]->GetBlockNumber();
		fwrite(&i, 4, 1, fp);
		fwrite(&bn, 4, 1, fp);
	}
	fclose(fp);
	cout << "Hash file created in format (table number, block number)" << endl;
}

int HashMap::GetBlockNumber(int _key) {
	return HashFunc(_key);
}
void HashMap::PrintHashTable() {
	
	cout << "-----------  Print " 
		<< mTableName << " Hash Table ----------------\n";
	for (int i = 0; i < mTableSize; i++) {
		cout << "\t\tTable[" << i << "] 's Block Number: "
			<< mTable[i]->GetBlockNumber() << endl;
	}
	cout << "--------------------------------------------\n";
}

//*********************************** Utils ************************************

void sort(int* arr, int len) {
	for (int i = 0; i < len; i++) {
		for (int j = i + 1; j < len; j++) {
			if (arr[i] > arr[j]) {
				int temp;
				temp = arr[j];
				arr[j] = arr[i];
				arr[i] = temp;
			}
		}
	}
}