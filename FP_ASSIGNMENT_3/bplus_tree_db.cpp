#include "bplus_tree_db.h"
#include <assert.h>
#include <cstdlib>

using namespace std;
//nodeID internal(0) #elements cf key childID
//nodeID leaf(1) #elements nextNode key blockNum
// isLeaf == 1 -> leaf node , 0 -> internal node

void loadBlock(string tableName, int nodeID, BPT_Node* node, int &root_num) {
	FILE *lp;

	lp = fopen((tableName + ".idx").c_str(), "rb");
	if (lp == NULL) {
		cout << "load file error" << endl;
	}
	else {
		fpos_t pos = s_block * (nodeID - 1);
		fsetpos(lp, &pos);

		int isLeaf, resultId, num_element, next, block;
		int cfId, cId;
		float key;

		fread(&resultId, sizeof(int), 1, lp);
		if (resultId != nodeID) {
			cout << "resultID: " << resultId << ", nodeID: " << nodeID << endl;
			cout << "result_ID != nodeID" << endl;
		}
		else {
			fread(&isLeaf, sizeof(int), 1, lp);
			fread(&num_element, sizeof(int), 1, lp);
			node->mId = nodeID;
			if (nodeID == root_num) {
				node->mRoot = true;
			}

			if (isLeaf == 0) { // internal node
				fread(&cfId, sizeof(int), 1, lp);
				node->c_first = cfId;
				for (int i = 0; i < num_element; i++) {
					fread(&key, sizeof(float), 1, lp);
					fread(&cId, sizeof(int), 1, lp);
					internal_element i_element(key, cId);
					node->i_Element.push_back(i_element);
				}
				node->entry_num = node->i_Element.size();
			}
			else { // leaf node
				fread(&next, sizeof(int), 1, lp);
				node->mLeaf = true;
				node->nextId = next;
				for (int i = 0; i < num_element; i++) {
					fread(&key, sizeof(float), 1, lp);
					fread(&block, sizeof(int), 1, lp);
					leaf_element l_element(key, block);
					node->l_Element.push_back(l_element);
				} // end of for
				node->entry_num = node->l_Element.size();
			} // end of else
		}
		fclose(lp);
	}
	
}

void firstInsert(string tableName, float key, int block, int &num_nodes, int &root_num) {
	FILE *fp;
	int isLeaf = 1;
	int childID = init_c;
	int end_block = init_end;
	int num_elements = 1;
	int zero = 0;
	fp = fopen((tableName + ".idx").c_str(), "wb+");
	if (fp == NULL) {
		cout << "wb mode file error" << endl;
	}
	num_nodes++;
	fwrite(&num_nodes, sizeof(int), 1, fp);
	fwrite(&isLeaf, sizeof(int), 1, fp);
	fwrite(&num_elements, sizeof(int), 1, fp);
	fwrite(&end_block, sizeof(int), 1, fp);
	fwrite(&key, sizeof(float), 1, fp);
	fwrite(&block, sizeof(int), 1, fp);
	int num_zero = (s_block - sizeof(int)*(4 + num_elements * 2)) / 4;
	for (int i = 0; i < num_zero; i++)
		fwrite(&zero, sizeof(int), 1, fp);

	root_num = 1;
	fclose(fp);
}

void updateNode(string tableName, BPT_Node* node, bool isNew, int &root_num) {
	static int cnt = 0;
	// isNew == 1 -> add new Node
	// isNew == 0 -> overwrite Node
	cnt++;
	if (node->mId < 0) {
		cout << "mId: " << node->mId << endl;
	}
	if (node->mRoot)
		root_num = node->mId;
	FILE* fp;
	fp = fopen((tableName + ".idx").c_str(), "rb+");
	fpos_t pos;

	if (fp == NULL) {
		cout << "CNT: " << cnt << endl;
		cout << "Update file error" << endl;
		assert(false);
		
	}
	else {
		if (!isNew) {
			pos = s_block*((node->mId) - 1);
			fsetpos(fp, &pos);
		}
		if (isNew) {
			fseek(fp, 0, SEEK_END);
			pos = ftell(fp);
		}
		int num_element, num_zero;
		int isLeaf = 0;
		int zero = 0;
		float key;
		
		if (node->mLeaf)
			isLeaf = 1;

		fwrite(&(node->mId), sizeof(int), 1, fp);
		fwrite(&isLeaf, sizeof(int), 1, fp);

		if (node->mLeaf) { // leaf node
			int block;
			list<leaf_element>::iterator it;
			num_element = node->l_Element.size();
			fwrite(&num_element, sizeof(int), 1, fp);
			fwrite(&(node->nextId), sizeof(int), 1, fp);

			for (it = node->l_Element.begin(); it != node->l_Element.end(); it++) {
				key = (*it).mKey;
				block = (*it).mBlock;
				fwrite(&key, sizeof(float), 1, fp);
				fwrite(&block, sizeof(int), 1, fp);
			}
		}
		else { // internal node
			int cId;
			list<internal_element>::iterator it;
			num_element = node->i_Element.size();
			fwrite(&num_element, sizeof(int), 1, fp);
			fwrite(&(node->c_first), sizeof(int), 1, fp);

			for (it = node->i_Element.begin(); it != node->i_Element.end(); it++) {
				key = (*it).mKey;
				cId = (*it).mChild;
				fwrite(&key, sizeof(float), 1, fp);
				fwrite(&cId, sizeof(int), 1, fp);
			}
		}
		num_zero = (s_block - sizeof(int)*(4 + num_element * 2)) / 4;
		for (int i = 0; i < num_zero; i++)
			fwrite(&zero, sizeof(int), 1, fp);
		fclose(fp);
	}

}

void Insert_Data(string tableName, float key, int block, int &root_num, int &num_nodes, queue<BPT_Node*> &update, queue<BPT_Node*> &n_update) {
	BPT *A = new BPT();
	if (root_num == 0) {
		firstInsert(tableName, key, block, root_num, num_nodes);
	}
	else {
		BPT_Node *root = new BPT_Node();
		loadBlock(tableName, root_num, root, root_num);
		A->root = root;
		root_num = A->root->mId;
		if (!A->root->mLeaf) {
			RecursiveNode(tableName, A->root, key, root_num, num_nodes);
		}
		A->insert_Record(key, block, num_nodes, update, n_update);
		//A->Print_Tree();
	}
	int u_size = update.size();
	int n_size = n_update.size();
	BPT_Node *up_node;
	for (int i = 0; i < u_size; i++) {
		up_node = update.front();
		updateNode(tableName, up_node, true, root_num);
		update.pop();
	}
	for (int i = 0; i < n_size; i++) {
		up_node = n_update.front();
		updateNode(tableName, up_node, false, root_num);
		n_update.pop();
	}
	delete A;
}

void RecursiveNode(string tableName, BPT_Node* tmp, float key, int &root_num, int &num_nodes) {
	list<internal_element> ilist = tmp->i_Element;
	list<internal_element>::iterator lit = tmp->i_Element.begin();
	list<internal_element>::iterator endprev = tmp->i_Element.end();
	endprev--;
	list<internal_element>::iterator next = tmp->i_Element.begin();
	next++;
	BPT_Node *child = new BPT_Node();
	int child_id = 0;
	for (lit = tmp->i_Element.begin(); lit != tmp->i_Element.end(); lit++) {
		if (key < tmp->i_Element.begin()->mKey) {
			child_id = tmp->c_first;
			break;
		}
		else if (key == lit->mKey) {
			child_id = lit->mChild;
			break;
		}
		else if (key > lit->mKey && lit == endprev) {
			child_id = lit->mChild;
			break;
		}
		else if (key > lit->mKey && key < next->mKey) {
			child_id = lit->mChild;
			break;
		}
		next++;
	}
	loadBlock(tableName, child_id, child, root_num);
	tmp->c_ptr = child;
	child->parent = tmp;
	if (!child->mLeaf) {
		RecursiveNode(tableName, child, key, root_num, num_nodes);
	}

}

void Range_Search(string tableName, float _min, float _max, int &root_num, int &num_nodes) {
	BPT *A = new BPT();
	BPT_Node *root = new BPT_Node();
	loadBlock(tableName, root_num, root, root_num);
	A->root = root;
	root_num = A->root->mId;
	if (!A->root->mLeaf) {
		RecursiveNode(tableName, A->root, _min, root_num, num_nodes);
	}
	ConnectLeaf(tableName, _max, root_num, A->findLeafNode(A->root));
	cout << "Range Search " << _min << " <= Record" << "<=" << _max << endl;
	A->Range_Search(_min, _max);
	delete A;
}

void ConnectLeaf(string tableName, float _max, int &root_num, BPT_Node* start) {
	BPT_Node *tmp = start;
	while (1) {
		list<leaf_element>::iterator lit1 = tmp->l_Element.end();
		lit1--;
		BPT_Node *nextN = new BPT_Node();

		if (_max < (*lit1).mKey)
			break;
		if (tmp->nextId < 0) {
			break;
		}
		else {
			loadBlock(tableName, tmp->nextId, nextN, root_num);
			tmp->next = nextN;
			tmp = tmp->next;
		}
	}
}