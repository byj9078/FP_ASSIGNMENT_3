#include "bplus_tree.h"


leaf_element::leaf_element(float _key, int _block) {
	mKey = _key;
	mBlock = _block;
}

internal_element::internal_element(float _key, int _child) {
	mKey = _key;
	mChild = _child;
}
BPT_Node::BPT_Node() {
	mId = init_id;
}

BPT_Node::BPT_Node(bool _leaf, bool _root, int _id, float _key, int _block) {
	mId = _id;
	mRoot = _root;
	mLeaf = _leaf;
	if (_leaf) {
		leaf_element tmp = leaf_element(_key, _block);
		l_Element.push_back(tmp);
	}
	else {
		internal_element tmp = internal_element(_key, init_c);
		i_Element.push_back(tmp);
	}
	entry_num = 1;
}

int BPT_Node::insert_Entry(float _key, int _BorC) {
	int count = 0;
	if (mLeaf) {
		list<leaf_element>::iterator it = l_Element.begin();
		leaf_element tmp = leaf_element(_key, _BorC);
		while (it != l_Element.end()) {
			if (it->mKey < _key) {
				it++;
				count++;
			}
			else {
				break;
			}
		}
		l_Element.insert(it, tmp);
	}
	else {
		list<internal_element>::iterator it = i_Element.begin();
		internal_element tmp = internal_element(_key, _BorC);
		while (it != i_Element.end()) {
			if (it->mKey < _key) {
				it++;
				count++;
			}
			else {
				break;
			}
		}
		i_Element.insert(it, tmp);
	}
	entry_num++;

	return count;
}

BPT::BPT(float _key, int _block) {
	root = new BPT_Node(true, true, init_id, _key, _block);
}

BPT::BPT() {}

BPT_Node* BPT::findLeafNode(BPT_Node *tmp) {
	if (tmp->mLeaf) {
		return tmp;
	}
	else {
		return findLeafNode(tmp->c_ptr);
	}
}

void BPT::insert_Record(float _key, int _block, int &count, queue<BPT_Node*> &update, queue<BPT_Node*> &n_update) {
	if (root == NULL) {
		BPT_Node *n_root = new BPT_Node(true, true, ++count, _key, _block);
		update.push(n_root);
		root = n_root;
	}
	BPT_Node *position = findLeafNode(root);
	position->insert_Entry(_key, _block);
	if (position->entry_num > BF) {
		split_leafNode(position, count, update, n_update);
	}
	else {
		n_update.push(position);
	}

}


void BPT::split_leafNode(BPT_Node *tmp, int &count, queue<BPT_Node*> &update, queue<BPT_Node*> &n_update) {
	list<leaf_element> l_element;
	list<leaf_element> r_element;
	list<leaf_element>::iterator e_lit = tmp->l_Element.begin();
	advance(e_lit, M);
	float key = e_lit->mKey;
	int block = e_lit->mBlock;
	ListSplice(&(tmp->l_Element), &l_element, &r_element);

	if (tmp->mRoot) {
		BPT_Node *new_r = new BPT_Node(true, false, ++count, key, block);
		BPT_Node *new_root = new BPT_Node(false, true, ++count, key, init_b);

		tmp->mRoot = false;
		tmp->l_Element.assign(l_element.begin(), l_element.end());
		tmp->entry_num = l_element.size();
		tmp->parent = new_root;
		tmp->next = new_r;
		tmp->nextId = new_r->mId;

		root = new_root;
		new_r->parent = new_root;
		new_r->l_Element.assign(r_element.begin(), r_element.end());
		new_r->entry_num = r_element.size();

		new_root->c_ptr = tmp;
		new_root->split_ptr = new_r;
		new_root->c_first = tmp->mId;
		new_root->i_Element.begin()->mChild = new_r->mId;
		update.push(new_r);
		update.push(new_root);
		n_update.push(tmp);
	}
	else {
		BPT_Node *new_r = new BPT_Node(true, false, ++count, key, block);
		new_r->parent = tmp->parent;
		new_r->l_Element.assign(r_element.begin(), r_element.end());
		new_r->entry_num = r_element.size();

		tmp->l_Element.assign(l_element.begin(), l_element.end());
		tmp->next = new_r;
		new_r->nextId = tmp->nextId;
		tmp->nextId = new_r->mId;
		tmp->entry_num = l_element.size();

		tmp->parent->insert_Entry(key, new_r->mId);
		tmp->parent->split_ptr = new_r;

		update.push(new_r);
		n_update.push(tmp);
		n_update.push(tmp->parent);

		if (tmp->parent->entry_num > BF) {
			split_internalNode(tmp->parent, count, update, n_update);
		}
	}
}


void  BPT::split_internalNode(BPT_Node *tmp, int &count, queue<BPT_Node*> &update, queue<BPT_Node*> &n_update) {
	// score split
	list<internal_element> l_element;
	list<internal_element> r_element;
	list<internal_element>::iterator e_lit = tmp->i_Element.begin();
	advance(e_lit, M);
	float key = e_lit->mKey;
	ListSplice(&(tmp->i_Element), &l_element, &r_element);


	// split left & right node
	tmp->i_Element = l_element;
	tmp->entry_num = l_element.size();
	BPT_Node *r_node = new BPT_Node(false, false, ++count, key, init_b);
	r_node->i_Element = r_element;
	r_node->entry_num = r_element.size();

	// chile ptr �����ֱ�
	e_lit = l_element.begin();
	bool one_left = false;
	bool two_left = false;
	BPT_Node *one_child = tmp->c_ptr;
	BPT_Node *two_child = tmp->split_ptr;
	tmp->c_ptr = NULL;
	tmp->split_ptr = NULL;
	r_node->c_ptr = NULL;
	r_node->split_ptr = NULL;
	for (e_lit = l_element.begin(); e_lit != l_element.end(); e_lit++) {
		if (e_lit->mChild == one_child->mId) {
			one_left = true;
			if (tmp->c_ptr == NULL) {
				tmp->c_ptr = one_child;
			}
			else {
				tmp->split_ptr = one_child;
			}
			one_child->parent = tmp;
		}
		if (tmp->split_ptr != NULL && (e_lit->mChild == tmp->split_ptr->mId)) {
			two_left = true;
			if (tmp->c_ptr == NULL) {
				tmp->c_ptr = two_child;
			}
			else {
				tmp->split_ptr = two_child;
			}
			two_child->parent = tmp;
		}
	}

	if (!one_left) {
		if (r_node->c_ptr == NULL) {
			r_node->c_ptr = one_child;
		}
		else {
			r_node->split_ptr = one_child;
		}
		one_child->parent = r_node;
	}
	if (!two_left && two_child != NULL) {
		if (r_node->c_ptr == NULL) {
			r_node->c_ptr = two_child;
		}
		else {
			r_node->split_ptr = two_child;
		}
		two_child->parent = r_node;
	}

	if (tmp->mRoot) {
		tmp->mRoot = false;
		BPT_Node *new_root = new BPT_Node(false, true, ++count, key, init_b);
		root = new_root;
		new_root->c_first = tmp->mId;
		new_root->i_Element.begin()->mChild = r_node->mId;
		tmp->parent = new_root;
		r_node->parent = new_root;
		new_root->c_ptr = tmp;
		new_root->split_ptr = r_node;
		update.push(r_node);
		update.push(new_root);
		n_update.push(tmp);
	}
	else {
		tmp->parent->insert_Entry(key, r_node->mId);
		tmp->parent->split_ptr = r_node;
		r_node->parent = tmp->parent;
		update.push(r_node);
		n_update.push(tmp);
		n_update.push(tmp->parent);
		if (tmp->parent->entry_num > BF) {
			split_internalNode(tmp->parent, count, update, n_update);
		}
	}
}

template <typename T>
void ListSplice(list<T>* _element, list<T> *l_element, list<T> *r_element) {
	list<T>::iterator lit = _element->begin();
	for (int i = 0; i < (_element->size()); i++) {
		if (i < M) {
			l_element->push_back(*lit);
		}
		else {
			r_element->push_back(*lit);
		}
		lit++;
	}
}

void BPT::Print_Tree() {
	cout << "\n\n << PRINT TREE >> " << endl;
	cout << "Print Root Node " << endl;
	Print_Node(root);
}

void BPT::Print_Node(BPT_Node* tmp) {
	cout << "ID : " << tmp->mId << endl;
	if (tmp->mLeaf) {
		cout << "Print Leaf Node" << endl;
		list<leaf_element>::iterator e_lit = tmp->l_Element.begin();
		for (e_lit = tmp->l_Element.begin(); e_lit != tmp->l_Element.end(); e_lit++) {
			cout << "key : " << e_lit->mKey << " block : " << e_lit->mBlock << "\t";
		}
		cout << endl;
		cout << "------------------------------" << endl;
	}
	else {
		cout << "Print InterNal Node" << endl;
		if (tmp->c_first != init_c) {
			cout << "child_id : " << tmp->c_first << " ";
		}
		list<internal_element>::iterator lit = tmp->i_Element.begin();
		for (lit = tmp->i_Element.begin(); lit != tmp->i_Element.end(); lit++) {
			cout << "key : " << lit->mKey << " child_id : " << lit->mChild << " ";
		}
		cout << endl;
		cout << "------------------------------" << endl;
		if (tmp->c_ptr != NULL) {
			Print_Node(tmp->c_ptr);
		}
		if (tmp->split_ptr != NULL) {
			Print_Node(tmp->split_ptr);
		}
	}
}

void BPT::Range_Search(float _min, float _max) {
	BPT_Node *leaf = findLeafNode(root);
	BPT_Node *next = leaf->next;
	list<leaf_element>::iterator lit;
	int c = 0;
	while (true) {
		bool Max = false;
		lit = leaf->l_Element.begin();
		for (int i = 0; i < leaf->entry_num; i++) {
			if (lit->mKey > _max) {
				Max = true;
				break;
			}
			if (lit->mKey >= _min) {
				cout << "| key : " << lit->mKey << " block : " << lit->mBlock << " |";
				c++;
				if (c % 4 == 0) {
					cout << endl;
				}
			}
		}
		if (next == NULL || Max) {
			break;
		}
		leaf = next;
		next = leaf->next;
		lit++;
	}
}

void BPT::delete_Node(BPT_Node* tmp) {
	if (tmp->mLeaf) {
		delete tmp;
	}
	else {
		if (tmp->c_ptr != NULL) {
			delete_Node(tmp->c_ptr);
		}
		if (tmp->split_ptr != NULL) {
			delete_Node(tmp->split_ptr);
		}
		delete tmp;
	}
}

BPT::~BPT() {
	if (root != NULL) {
		delete_Node(root);
	}
}
