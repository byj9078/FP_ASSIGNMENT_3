#ifndef QUERYHELPER_H
#define QUERYHELPER_H

#include "DBHelper.h"

class QueryHelper {
private:
	
	DBHelper* mStudDB;
	DBHelper* mProfDB;
public:
	QueryHelper();
	void GetInfoData();
	void GetQueryData();
	void AnalyzeQueryState(string query);
	void SearchExact(string _tableName, string _att, int _key);
	void SearchRange(string _tableName, float _min, float _max);
	void Join(string _tableName1, string _tableName2);
};

#define QUERY_FILE_NAME "query.dat"
#define QUERY_RESULT_FILE_NAME "query.res"
#endif //QUERYHELPER_H


