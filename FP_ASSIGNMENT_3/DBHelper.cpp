#include "DBHelper.h"
#include <assert.h>

DBHelper::DBHelper(string _table_name)
	: mEntrySize(0)
	, mTableName(_table_name) {
	mHashMap = new HashMap(_table_name);
	mBplusTree = new BPT();
	FILE* mFileDB = fopen((mTableName + ".db").c_str(), "wb+");
	fclose(mFileDB);
	mSnum_nodes = 0;
	mSroot_num = 0;
	mPnum_nodes = 0;
	mProot_num = 0;
}

void DBHelper::GetStudDataFromFile() {
	ifstream fin(INPUT_STUD_FILE_NAME);
	if (fin.fail()) {
		cout << "Could not find " << INPUT_STUD_FILE_NAME << endl;
		exit(0);
	}
	string sData;
	stringstream ss;

	getline(fin, sData);
	ss.str(sData);
	ss >> mEntrySize;
	cout << "\n\n\n\t\t\t Getting Student's Data. Please wait...." << endl;	
	int size = mEntrySize;

	queue<BPT_Node*> update; // for BPT
	queue<BPT_Node*> n_update;

	for (int i = 0; i < size; i++) {
		string sData2;
		stringstream ss2;
		string split_by_comma[4];
		int dataForHash;
		float dataForBPT;

		getline(fin, sData2);
		ss2.str(sData2);
		SplitByComma(sData2, split_by_comma);

		dataForHash = atoi(split_by_comma[1].c_str());
		dataForBPT = atof(split_by_comma[2].c_str()); // score?

		mHashMap->Insert(dataForHash);
		InsertStudDataIntoDB(StudentInfo(sData2));
		Insert_Data("Student_Score", dataForBPT, mHashMap->GetBlockNumber(dataForHash), mSroot_num, mSnum_nodes, update, n_update);
	}
	
	cout << "\n\t\t\tDone!!" << endl;
	//int k;

	//mHashMap->PrintHashTable();
	//mHashMap->PrintStudDBFile();
	//mBplusTree->CreateIdxFile();
	//cout << "Enter the integer showing k'th leaf node : ";
	//cin >> k;
	//mBplusTree->showKthLeaf(k);
}
void DBHelper::GetProfDataFromFile() {
	ifstream fin(INPUT_PROF_FILE_NAME);
	if (fin.fail()) {
		cout << "Could not find " << INPUT_PROF_FILE_NAME << endl;
		exit(0);
	}
	string sData;
	stringstream ss;

	getline(fin, sData);
	ss.str(sData);
	ss >> mEntrySize;
	
	cout << "\n\n\n\t\t\t Getting Professor's Data. Please wait...." << endl;
	int size = 1000;
	for (int i = 0; i < size; i++) {
		string sData2;
		stringstream ss2;
		string split_by_comma[3];
		int dataForHash;
		float dataForBPT;

		queue<BPT_Node*> update;
		queue<BPT_Node*> n_update;

		getline(fin, sData2);
		ss2.str(sData2);
		SplitByComma(sData2, split_by_comma);

		dataForHash = atoi(split_by_comma[1].c_str()); // ID
		dataForBPT = atoi(split_by_comma[2].c_str()); // salary

		mHashMap->Insert(dataForHash);
		InsertProfDataIntoDB(ProfessorInfo(sData2));
		Insert_Data("Professor_Salary", dataForBPT, mHashMap->GetBlockNumber(dataForHash), mProot_num, mPnum_nodes, update, n_update);
		//mHashMap->PrintProfDBFile();

	}
	cout << "\n\t\t\tDone!!" << endl;
	//int k;

	//mHashMap->PrintHashTable();
	//mHashMap->PrintProfDBFile();
	//mBplusTree->CreateIdxFile();
	//cout << "Enter the integer showing k'th leaf node : ";
	//cin >> k;
	//mBplusTree->showKthLeaf(k);
}
void DBHelper::InsertStudDataIntoDB(StudentInfo _stud) {
	FILE* mFileDB = fopen((mTableName + ".db").c_str(), "rb+");
	assert(mFileDB);
	int hash = mHashMap->HashFunc(_stud.mStudentID);
	int curNumEntry = mHashMap->mTable[hash]->GetNumEntry();
	fpos_t pos = 4096 * mHashMap->mTable[hash]->GetBlockNumber() + (curNumEntry - 1) * 32;
	fsetpos(mFileDB, &pos);
	
	fwrite(&_stud, sizeof(StudentInfo), 1, mFileDB);
	fclose(mFileDB);
}
void DBHelper::InsertProfDataIntoDB(ProfessorInfo _prof) {
	FILE* mFileDB = fopen((mTableName + ".db").c_str(), "rb+");
	assert(mFileDB);
	int hash = mHashMap->HashFunc(_prof.mProfID);
	int curNumEntry = mHashMap->mTable[hash]->GetNumEntry();
	fpos_t pos = 4096 * mHashMap->mTable[hash]->GetBlockNumber() + (curNumEntry - 1) * 28;
	fsetpos(mFileDB, &pos);

	fwrite(&_prof, sizeof(ProfessorInfo), 1, mFileDB);
	fclose(mFileDB);

}
void DBHelper::CreateHashFile() {
	mHashMap->CreateHashFile();
}
/*
void DBHelper::CreateIndexFile() {
	//mBplusTree->CreateIdxFile();
}*/

DBHelper::~DBHelper() {
	delete mHashMap;
	delete mBplusTree;
}

