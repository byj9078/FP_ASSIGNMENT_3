#ifndef PROFESSORINFO_H
#define PROFESSORINFO_H


#include <cstring>
#include <cstring>
#include <sstream>
#include <iostream>
using namespace std;

class ProfessorInfo {
public:
	char mName[20];
	unsigned mProfID;
	int mSalary;
	ProfessorInfo() {};
	ProfessorInfo(char _name[20], unsigned _profID,
		int _salary);
	ProfessorInfo(const ProfessorInfo& _prof);
	ProfessorInfo(string sData);
	~ProfessorInfo();

};

#endif //  PROFESSORINFO_H
