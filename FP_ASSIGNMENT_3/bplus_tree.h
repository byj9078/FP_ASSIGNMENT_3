#include <iostream>
#include <list>
#include <queue>
#include <iterator>
#include <fstream>
#define BF 4096/32
#define init_id -1
#define init_b -2
#define init_c -3
#define init_n -4
#define init_end -5;

#if BF%2
#define M BF/2+1
#else
#define M BF/2
#endif

#ifndef BPLUS_TREE_H_INCLUDED
#define BPLUS_TREE_H_INCLUDED



using namespace std;
using namespace std;

class leaf_element {
public:
	float mKey;
	int mBlock;
	leaf_element(float _key, int _block);
};

class internal_element {
public:
	float mKey;
	int mChild;
	internal_element(float _key, int _child);
};

class BPT_Node {
public:
	int entry_num;
	bool mRoot = false;
	bool mLeaf = false;
	int mId;
	int nextId;
	list<leaf_element> l_Element;
	int c_first = init_c;
	list<internal_element> i_Element;
	BPT_Node *c_ptr = NULL;
	BPT_Node *split_ptr = NULL;
	BPT_Node *next = NULL;
	BPT_Node *parent = NULL;
	BPT_Node();
	BPT_Node(bool _leaf, bool _root, int _id, float _key, int _block);
	int insert_Entry(float _key, int _BorC);

};


class BPT {
public:
	BPT_Node *root = NULL;
	BPT();
	BPT(float _key, int _block);
	void insert_Record(float _key, int _block, int &count, queue<BPT_Node*> &update, queue<BPT_Node*> &n_update);
	void split_leafNode(BPT_Node *tmp, int &count, queue<BPT_Node*> &update, queue<BPT_Node*> &n_update);
	void split_internalNode(BPT_Node *tmp, int &count, queue<BPT_Node*> &update, queue<BPT_Node*> &n_update);
	BPT_Node *findLeafNode(BPT_Node *tmp);
	void Print_Tree();
	void Print_Node(BPT_Node *tmp);
	void Range_Search(float _min, float _max);
	void delete_Node(BPT_Node *tmp);
	~BPT();
};

template <typename T>
void ListSplice(list<T>* _element, list<T> *l_element, list<T> *r_element);
#endif