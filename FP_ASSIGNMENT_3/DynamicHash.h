#ifndef DYNAMICHASH_H
#define DYNAMICHASH_H

#include <iostream>
#include <math.h>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <sstream>
#include <fstream>
#include "StudentInfo.h"
#include "ProfessorInfo.h"
using namespace std;


//************************************Bucket************************************

class Bucket {
private:
	friend class QueryHelper;
	int mNumEntry;
	int mBucketSize;
	int mLocalDepth;
	int mBlockNumber;
	string mTableName;
public:
	Bucket(int _localdepth, string _tablename);

	int GetBlockNumber() const;
	void SetBlockNumber(int _num);

	void SetNumEntry(int _numentry);
	bool IsFull() const;
	int GetNumEntry() const;
	int GetBucketSize() const;
	int GetLocalDepth() const;
	void SetLocalDepth(int _depth);
	void IncreaseNum();
	string GetTableName() const;
};

//************************************HashMap************************************
class HashMap {
private:
	friend class QueryHelper;
	friend class DBHelper;
	string mTableName;
	
	int mTableSize;
	int mGlobalDepth;
	int mWholeNumEntry;
	Bucket** mTable;

	
	fstream mFileHash;
	int HashFunc(unsigned _key);
	
public:
	
	HashMap(string _tablename);
	void Insert(unsigned _key);
	
	~HashMap();

	void IncreaseHashTable();
	void PrintStudDBFile();
	void PrintProfDBFile();
	void PrintHashTable();
	int GetBlockNumber(int _key);
	void SplitBucket(int _oldTableNum, unsigned _key);
	void TableDestructor(int _tablenum);
	void RedistributeStud(int _oldtablenum, Bucket* bucket1, Bucket* bucket2);
	void RedistributeProf(int _oldtablenum, Bucket* bucket1, Bucket* bucket2);
	void CreateHashFile();
};
//*********************************** Utils ************************************
#define DEFAULT_TABLE_SIZE 2
#define DEFAULT_STUD_BUCKET_SIZE  (int)(4096 / (32))
#define DEFAULT_PROF_BUCKET_SIZE  (int)(4096 / (28))
// (int)(4096 / (NAMELENGTH + 12))
// block size / (30 + 4 + 4 + 4 + 3(3commas))

#endif //  DYNAMICHASH_H
